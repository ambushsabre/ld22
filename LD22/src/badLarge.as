package  
{
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	import org.flixel.*;
	public class badLarge extends FlxSprite 
	{
		[Embed(source = "../largeEnemy.png")] protected var Sprite:Class;
		[Embed(source="../EnemyShoot.mp3")]
		protected var Shoot:Class;
		[Embed(source="../bigguyExplosion.mp3")]
		protected var Explode:Class;
		private var shootInterval:int;
		private var shootCounter:int = 0;
		public function badLarge(_x:int, _y:int) 
		{
			health = 5;
			shootInterval = 1 * 60;
			x = _x;
			y = _y;
			loadGraphic(Sprite, false, false, 15, 15);
			Registry.enemies.add(this);
		}
		
		private function bulletHit(obj1:FlxSprite, obj2:FlxSprite):void 
		{
			FlxG.play(Explode);
			flicker(.5);
			health -= 1;
			if (health <= 0)
				obj1.kill();
			obj2.kill();
			Registry.score += 200;
		}
		
		override public function update():void 
		{
			shootCounter += 1;
			if (shootCounter >= shootInterval)
			{
				FlxG.play(Shoot);
				FlxG.state.add(new bullet(x, y, 1, 1, false));
				FlxG.state.add(new bullet(x, y, 2, 2, false));
				FlxG.state.add(new bullet(x, y, 3, 3, false));
				FlxG.state.add(new bullet(x, y, -1, 1, false));
				FlxG.state.add(new bullet(x, y, -2, 2 , false));
				FlxG.state.add(new bullet(x, y, -3, 3 , false));
				shootCounter = 0;
			}
			FlxG.overlap(this, Registry.playerBullets, bulletHit);
			y += .5;
			if (y >= 250)
			{
				this.kill();
			}
		}
	}
}