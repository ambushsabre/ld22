package
{
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class pupShield extends FlxSprite
	{
		[Embed(source="../pupshield.png")]
		protected var Sprite:Class;
		[Embed(source="../powerup.mp3")]
		protected var Powerup:Class;
		
		public function pupShield(_x:int, _y:int)
		{
			x = _x;
			y = _y;
			loadGraphic(Sprite, false, false, 16, 16);
		}
		
		private function playerHit(obj1:FlxSprite, obj2:FlxSprite):void
		{
			FlxG.play(Powerup);
			obj1.kill();
			if (Registry.player.health != 2)
			{
				Registry.pilotText.x = 130;
				Registry.pilotText.text = "Shields Recharged!";
				Registry.player.health = 2;
				Registry.shieldTut = true;
			}
			else
				Registry.score += 2000;
		}
		
		override public function update():void
		{
			FlxG.overlap(this, Registry.player, playerHit);
			y += .75;
			if (y >= 250)
			{
				this.kill();
			}
		}
	}

}