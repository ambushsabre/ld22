package
{
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	import org.flixel.*;
	
	public class badMove extends FlxSprite
	{
		[Embed(source="../moveship.png")]
		protected var Sprite:Class;
		[Embed(source="../EnemyShoot.mp3")]
		protected var Shoot:Class;
		[Embed(source="../smallExplode.mp3")]
		protected var Explode:Class;
		private var shootInterval:int;
		private var shootCounter:int = 0;
		
		public function badMove(_x:int, _y:int)
		{
			shootInterval = Math.round((FlxG.random() * 4) * 60);
			x = _x;
			y = _y;
			loadGraphic(Sprite, false, false, 16, 10);
			Registry.enemies.add(this);
		}
		
		private function bulletHit(obj1:FlxSprite, obj2:FlxSprite):void
		{
			FlxG.play(Explode);
			obj1.kill();
			obj2.kill();
			Registry.score += 250;
		}
		
		override public function update():void
		{
			shootCounter += 1;
			if (shootCounter >= shootInterval)
			{
				FlxG.play(Shoot);
				FlxG.state.add(new bullet(x, y, 0, 2, false));
				shootCounter = 0;
			}
			FlxG.collide(this, Registry.playerBullets, bulletHit);
			y += 1;
			if (y >= 250)
			{
				this.kill();
			}
		}
	}
}