package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class bullet extends FlxSprite
	{
		private var xdir:int, ydir:int;
		private var player:Boolean;
		[Embed(source = "../bullet.png")] protected var Sprite:Class;
		public function bullet(_x:int, _y:int, _xdir:int, _ydir:int, _player:Boolean) 
		{
			player = _player;
			xdir = _xdir;
			ydir = _ydir;
			x = _x;
			y = _y;
			loadGraphic(Sprite, false, false, 9, 9);
			if (player == true)
			{
				Registry.playerBullets.add(this);
			}
			else 
			{
				Registry.bullets.add(this);
			}
		}
		
		override public function update():void 
		{
			if (y < -10)
				kill();
			else if (y > 250)
				kill();
			
			x += xdir;
			y += ydir;
		}
	}

}