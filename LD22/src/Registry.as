package
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class Registry
	{
		public static var spawnCd:int = 0;
		public static var shootCd:int = 0;
		public static var clearsRemain:int = 3;
		
		public static var enemies:FlxGroup;
		public static var bullets:FlxGroup;
		public static var playerBullets:FlxGroup;
		
		public static var pilotText:FlxText;
		public static var movementTut:Boolean = false;
		public static var shootTut:Boolean = false;
		public static var shieldTut:Boolean = false;
		
		public static var player:Player;
		public static var score:int;
		
		public function Registry()
		{
		
		}
	
	}

}