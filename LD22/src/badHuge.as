package
{
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	import org.flixel.*;
	
	public class badHuge extends FlxSprite
	{
		[Embed(source="../badHuge.png")]
		protected var Sprite:Class;
		[Embed(source="../EnemyShoot.mp3")]
		protected var Shoot:Class;
		[Embed(source="../bigguyExplosion.mp3")]
		protected var Explode:Class;
		private var shootInterval:int;
		private var shootCounter:int = 0;
		
		public function badHuge(_x:int, _y:int)
		{
			health = 15;
			shootInterval = .15 * 60;
			x = _x;
			y = _y;
			loadGraphic(Sprite, false, false, 33, 33);
			Registry.enemies.add(this);
		}
		
		private function bulletHit(obj1:FlxSprite, obj2:FlxSprite):void
		{
			FlxG.play(Explode);
			flicker(.5);
			health -= 1;
			if (health <= 0)
				obj1.kill();
			obj2.kill();
			Registry.score += 500;
		}
		
		override public function update():void
		{
			shootCounter += 1;
			if (shootCounter >= shootInterval)
			{
				FlxG.play(Shoot);
				FlxG.state.add(new bullet(x + 9, y + 32, 0, 2, false));
				FlxG.state.add(new bullet(x + 9, y + 32, 1, 1, false));
				FlxG.state.add(new bullet(x + 9, y + 32, -1, 1, false));
				shootCounter = 0;
			}
			FlxG.overlap(this, Registry.playerBullets, bulletHit);
			y += .5;
			if (y >= 250)
			{
				this.kill();
			}
		}
	}
}