package
{
	
	import org.flixel.*;
	
	public class PlayState extends FlxState
	{
		//an arcade game where the player is alone in the world, and his own movement causes the game to happen
		private var txtScore:FlxText;
		
		override public function create():void
		{
			Registry.pilotText = new FlxText(160, 120, 200, "...");
			Registry.pilotText.alpha = .5;
			add(Registry.pilotText);
			
			Registry.enemies = new FlxGroup();
			Registry.bullets = new FlxGroup();
			Registry.playerBullets = new FlxGroup();
			
			Registry.player = new Player(100, 100);
			add(Registry.player);
			
			txtScore = new FlxText(0, 0, 100, "Score: " + Registry.score.toString(), false);
			add(txtScore);
		}
		
		public function updateScore():void
		{
			txtScore.text = "Score: " + Registry.score.toString();
		}
		
		override public function update():void
		{
			updateScore();
			super.update();
		}
	}
}

