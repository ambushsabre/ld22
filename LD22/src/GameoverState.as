package
{
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.*;
	
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class GameoverState extends FlxState
	{
		override public function create():void
		{
			var txtScore:FlxText = new FlxText(100, 0, 200, "Final Score: " + Registry.score.toString(), false);
			//txtScore.flicker( -1);
			add(txtScore);
			
			var txtRestart:FlxText = new FlxText(90, FlxG.height / 2, 200, "Press X to re-engange...", false);
			txtRestart.flicker(-1);
			add(txtRestart);
		}
		
		public function GameoverState()
		{
		
		}
		
		override public function update():void
		{
			if (FlxG.keys.justPressed("X"))
			{
				Registry.score = 0;
				FlxG.flash(0xFFFFFF, 2);
				FlxG.switchState(new PlayState);
			}
			super.update();
		}
	
	}

}