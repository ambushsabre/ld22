package
{
	import org.flixel.*;
	
	public class Player extends FlxSprite
	{
		[Embed(source="../player.png")]
		protected var Sprite:Class;
		[Embed(source="../PlayerShoot.mp3")]
		protected var Shoot:Class;
		private var speed:int = 3;
		private var totalBullets:int = 0;
		private var spawnRate:int = 35;
		private var pupSpawn:int = 0;
		
		public function Player(_x:int, _y:int)
		{
			health = 1;
			x = _x;
			y = _y;
			loadGraphic(Sprite, false, false, 7, 7);
		}
		
		override public function update():void
		{
			moveTut();
			shootTut();
			pupSpawn += 1;
			if (pupSpawn >= (15 * 60))
			{
				FlxG.state.add(new pupShield(FlxG.random() * 320, 0));
				pupSpawn = 0;
			}
			Registry.spawnCd += 1;
			//trace(Registry.spawnCd.toString());
			if (Registry.spawnCd >= spawnRate)
			{
				Registry.spawnCd = spawnRate;
			}
			//Movement code
			//Very crappy movement code, but it works ok!!
			//TODO: change this to velocity at some point to make collisions more accurate
			if (FlxG.keys.LEFT)
			{
				x -= speed;
				spawnMoveSide();
				Registry.movementTut = true;
			}
			if (FlxG.keys.RIGHT)
			{
				x += speed;
				spawnMoveSide();
				Registry.movementTut = true;
			}
			if (FlxG.keys.UP)
			{
				y -= speed;
				spawnMoveDown();
				Registry.movementTut = true;
			}
			if (FlxG.keys.DOWN)
			{
				y += speed;
				spawnMoveDown();
				Registry.movementTut = true;
			}
			
			if (x > 310)
				x = 310;
			if (x < 0)
				x = 0;
			if (y > 230)
				y = 230;
			if (y < 0)
				y = 0;
			
			if (FlxG.keys.justPressed("Z"))
			{
				shoot();
			}
			
			if (FlxG.keys.justPressed("X"))
			{
				clear();
			}
			
			FlxG.collide(this, Registry.bullets, bulletHit);
			super.update();
		}
		
		private function shootTut():void
		{
			if (Registry.movementTut == true && Registry.shootTut == false)
			{
				Registry.pilotText.x = 130;
				Registry.pilotText.text = "press Z to fire";
			}
		}
		
		private function moveTut():void
		{
			if (Registry.movementTut == false)
			{
				Registry.pilotText.x = 100;
				Registry.pilotText.text = "use the arrow keys to move";
			}
		}
		
		private function bulletHit(obj1:FlxSprite, obj2:FlxSprite):void
		{
			if (health == 1)
			{
				FlxG.flash(0xFF0000, 2);
				FlxG.switchState(new GameoverState);
				totalBullets = 0;
				Registry.clearsRemain = 3;
			}
			else
			{
				Registry.pilotText.x = 130;
				Registry.pilotText.text = "Shields Down!";
				health -= 1;
				obj2.kill();
			}
		}
		
		private function clear():void
		{
			if (Registry.clearsRemain > 0)
			{
				Registry.pilotText.x = 140;
				Registry.pilotText.text = "BOOM!";
				FlxG.flash(0xFFFFFF, 1);
				Registry.enemies.callAll("kill");
				FlxG.state.add(new badHuge(140, -16));
				Registry.clearsRemain -= 1;
			}
		}
		
		private function shoot():void
		{
			FlxG.play(Shoot, 1, false);
			Registry.shootTut = true;
			if (Registry.shieldTut == false)
			{
				Registry.pilotText.text = "your shields are down...find one in a bubble to recharge them";
				Registry.shieldTut = true;
			}
			FlxG.state.add(new bullet(x + 4, y - 3, 0, -4, true));
			totalBullets += 1;
			Registry.shootCd += 1;
			if (Registry.shootCd >= 20)
			{
				FlxG.state.add(new badLarge(FlxG.random() * 320, 0));
				Registry.shootCd = 0;
			}
			//make the dude shoot here
		}
		
		private function spawnMoveSide():void
		{
			if (Registry.spawnCd >= spawnRate)
			{
				FlxG.state.add(new badSide(Math.round(FlxG.random() * 1)));
				Registry.spawnCd = 0;
			}
		}
		
		private function spawnMoveDown():void
		{
			if (Registry.spawnCd >= spawnRate)
			{
				FlxG.state.add(new badMove(FlxG.random() * 320, 0));
				Registry.spawnCd = 0;
			}
		}
	}
}